const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
	new HtmlWebpackPlugin({
		template: './src/index.pug',
		filename: './index.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/find-customer-empty.pug',
		filename: './find-customer-empty.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/find-customer-list.pug',
		filename: './find-customer-list.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/customer-details.pug',
		filename: './customer-details.html'
	}),
]