export default class Notify {
	constructor(options) {
		this.title = options.title;
		this.message = options.message || '';
		this.notify = null;
		this.notifyContainer = document.querySelector('.notification-containter');

		this.create();
		this.addInContainer();
		this.show();

		setTimeout(() => {
			this.hide();
		}, 6e3);

		this.notifyContainer.addEventListener('click', e => {
			if( e.target.closest('.notification') != this.notify ) return 0;
				this.hide();
		})
	}

	create() {
		this.notify = document.createElement('div');
		this.notify.classList.add('notification');
		this.notify.innerHTML = `
			<div class="notification-wrap">
				<div class="notification__icon"><i class="icon-check"></i></div>
				<div class="notification-text">
					<div class="notification-text__title">${this.title}</div>
					<div class="notification-text__message">${this.message}</div>
				</div>
			</div>`;

		this.notify.style.display = 'block';
		this.notify.style.opacity = 0;
	}

	addInContainer() {
		this.notifyContainer.appendChild(this.notify);
	}

	show() {
		this.notify.classList.add('animated');
		this.notify.classList.add('fadeInUp');
	}

	hide() {
		this.notify.classList.remove('fadeInUp');
		this.notify.classList.add('fadeOutUp');
		
		setTimeout(() => {
			this.notify.remove();
		}, 500);
	}
}