import Notify from './notification';
import './../scss/styles.scss';

window.Notify = Notify;

document.addEventListener('click', function(e) {
	const target = e.target;

	if( !target.closest('.header__menu-toggler') ) return 0;
	
	const menu = document.querySelector('.header-menu');

	menu.style.opacity = 0;
	menu.style.display = 'block';
	menu.classList.add('animated');
	menu.classList.add('fadeInUp');
	
	hideScroll();

	e.preventDefault();
});

document.addEventListener('click', function(e) {
	const target = e.target;

	if( !target.closest('.header-menu__close') )  return 0;

	const menu = document.querySelector('.header-menu');

	menu.classList.remove('fadeInUp');
	menu.classList.add('animated');
	menu.classList.add('fadeOutUp');
	
	setTimeout(() => {
		menu.classList.remove('animated');
		menu.classList.remove('fadeOutUp');
		menu.style.display = 'none';
	}, 500);

	setTimeout(showScroll, 50);

	e.preventDefault();
});

// блокировка/разблокировка скрола
function hideScroll() {
	setTimeout(() => {
		if ( !document.body.hasAttribute('data-body-scroll-fix') ) {
			let scrollPosition = window.pageYOffset || document.documentElement.scrollTop;
			document.body.setAttribute('data-body-scroll-fix', scrollPosition);
			document.body.style.overflow = 'hidden';
			document.body.style.position = 'fixed';
			document.body.style.top = '-' + scrollPosition + 'px';
			document.body.style.left = '0';
			document.body.style.width = '100%';
		}
	}, 10 );
}

function showScroll() {
	if ( document.body.hasAttribute('data-body-scroll-fix') ) {
		const scrollPosition = document.body.getAttribute('data-body-scroll-fix');
		document.body.removeAttribute('data-body-scroll-fix');
		document.body.style.overflow = '';
		document.body.style.position = '';
		document.body.style.top = '';
		document.body.style.left = '';
		document.body.style.width = '';
		window.scroll(0, scrollPosition);
	}
}